# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.0

- minor: Updated debricked/cli to a version supporting unaffected vulnerabilities

## 0.2.5

- patch: Made compatible with latest version of debricked/cli

## 0.2.4

- patch: Bumped version number due to new version of debricked/cli

## 0.2.3

- patch: Bumped version number due to new version of debricked/cli

## 0.2.2

- patch: Only run scan once in GitLab CI integration

## 0.2.1

- patch: Minor fixes to GitLab CI integration

## 0.2.0

- minor: Added support for GitLab CI

## 0.1.1

- patch: Changed pipe icon to a square logo

## 0.1.0

- minor: Initial release

